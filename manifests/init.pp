# @summary A short summary of the purpose of this class
#
# A description of what this class does
#
# @example
#   include demo
class demo(
  Array[Integer] $array = [1,2,3]
) {

  $array.each | $value | {
    debug::break()
    file{"/tmp/test${value}.txt": ensure => present }
  }

}
