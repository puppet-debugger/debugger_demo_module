function demo::sum(Array[Integer] $arry) >> Integer {
  $arry.reduce | $acc, $value | {
    # debug::break()
    $acc + $value
  }
}
