require 'spec_helper'

describe 'demo' do
  let(:params) do
      {
        array: [1,2,3,4]
      }
  end
  on_supported_os.each do |os, os_facts|

    context "on #{os}" do
      let(:facts) { os_facts }
      

      it { is_expected.to compile }
      it { is_expected.to contain_file('/tmp/test1.txt')}
      it { is_expected.to contain_file('/tmp/test2.txt')}
      it { is_expected.to contain_file('/tmp/test3.txt')}
      it { is_expected.to contain_file('/tmp/test4.txt')}

    end
  end
end
