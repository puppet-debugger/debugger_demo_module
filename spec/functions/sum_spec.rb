require 'spec_helper'

describe 'demo::sum' do
  let(:arry) do
    [1,2,3]
  end
  it { is_expected.to run.with_params(arry).and_return(6) }
end
